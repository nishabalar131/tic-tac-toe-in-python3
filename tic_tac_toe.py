board = [' ' for x in range(10)]


def insertLetter(letter,pos):
    board[pos] = letter

def spaceIsFree(pos):
    return board[pos] == ' '

def boardFull(board):
    if board.count(' ') > 1:
        return False
    else:
        return True

def winner(board,l):
   return( board[1] == l and board[2] == l and board[3] == l) or (board[4] == l and board[5] == l and board[6] == l) or (board[7] == l and board[8] == l and board[9] == l) or (board[1] == l and board[5] == l and board[9] == l) or (board[3] == l and board[5] == l and board[7] == l) or (board[1] == l and board[4] == l and board[7] == l) or (board[2] == l and board[5] == l and board[8] == l) or (board[3] == l and board[6] == l and board[9] == l)

def playerMove():
    run = True
    while run:
        move = input("select a position to enter x between 1 to 9 :")
        try:
            move = int(move)
            if move > 0 and move < 10:
                if spaceIsFree(move):
                    run=False
                    insertLetter('x',move)
                else:
                    print('sorry,this space is occupied')
            else:
                print('please enter valid number')

        except:
            print('type a number')

def computerMove():
    possibleMoves = [x for x, letter in enumerate(board) if letter == ' ' and x != 0]
    move=0
    for let in ['o','x']:
        for i in possibleMoves:
            boardcopy = board[:]
            boardcopy[i] = let
            if winner(boardcopy,let):
               move = i
               return move

    cornerOpen = []
    for i in possibleMoves:
        if i in [1,3,7,9]:
            cornerOpen.append(i)

    if len(cornerOpen)>0:
        move=selectRandom(cornerOpen)
        return move

    if 5 in possibleMoves:
        move = 5
        return move

    edgeOpen = []
    for i in possibleMoves:
        if i in [2,4,6,8]:
            edgeOpen.append(i)

    if len(edgeOpen)>0:
        move=selectRandom(cornerOpen)
    return move
 

def selectRandom(li):
    x =[1,2,3]
    import random 
    ln = len(li)
    r = random.randrange(0,ln)
    return li[r]


def printBoard(board):
    print('   |   |   ')
    print(' '+ board[1] + ' | ' + board[2] + ' | ' + board[3])
    print('   |   |   ')
    print('------------')
    print('   |   |   ')
    print(' '+ board[4] + ' | ' + board[5] + ' | ' + board[6])
    print('   |   |   ')
    print('------------')
    print('   |   |   ')
    print(' '+ board[7] + ' | ' + board[8] + ' | ' + board[9])
    print('   |   |   ')

    
def main():
    print('welcome to the game')
    printBoard(board)

    while not(boardFull(board)):
        if not(winner(board,'o')):
            playerMove()
            printBoard(board)

        else:
            print('sorry you loosed')
            break

        if not(winner(board, 'x')):
            move = computerMove()
            if move == 0:
                print('tie')

            else:
                insertLetter('o',move)
                print('computer placed o at position', move,':')
                printBoard(board)

        else:
            print('you won!')
            break
            
    if boardFull(board):
        print('No space') 

main()

while True:
    x = input('do you wish to paly again?(y/n)')
    if x.lower() == 'y':
        board = [' ' for x in range(10)]
        print('-----------------------')
        main()

    else:
        break
 